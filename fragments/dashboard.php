<script>
/* login test */

let init = true;
let noteLoad = false;
let loginAttempt = false;
let notes = new Array();
user = new ajax('/API/login.php'); 
tasks = new ajax('/API/tasks.php');
note = new ajax('/API/notes.php');



user.call({action: "chck"});      
tasks.call({action: "getTasks"});

 
       
$(document).ajaxStop( function () { //po přijetí response na všechna ajax volání
    if (init) {
        if (user.response == "false" || user.response == "null") { // vynucení přihlášení
            if(loginAttempt) {
                loginAttempt = false;
                $("#userModal .modal-body").prepend("<div class=\"alert alert-danger\" role=\"alert\">Uživatelské jméno nebo heslo je nesprávné</div>");
            }
            $('#userModal').modal('show');
            console.log('uživatel není přihlášen');
        }
        else { // zalozeni uzivatele v hlavicce
            $('#userModal').modal('hide');
            let response = JSON.parse(user.response);
            $("#menu ul").html("<li><button type=\"button\" class=\"btn btn-primary\" onclick=\"$('#newTask').modal('show');\">Přidat nový úkol</button></li>");
            $("#user .navbar-nav").html("<li class=\"nav-item dropdown\"><a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdownMenuLink\" role=\"button\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">" + response.login + "</a><ul class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\"><li>" + response.jmeno + " " + response.prijmeni + "</li><li><a class=\"dropdown-item\" href=\"mailto:" + response.email + "\">" + response.email + "</a></li><li><a class=\"dropdown-item\" href=\"#\" onclick=\"logout();\">logout</a></li></ul></li>");
            $('.datepicker').datepicker({
                format: "yyyy-mm-dd"
            });
        }
        //alert(tasks.response);
        alltasks = JSON.parse(tasks.response); // tasky do pole objektů
        alltasks.sort(function (a,b) {
            return new Date(a.startdate) - new Date(b.startdate);
        });
        $("#tasks").html(""); //smazání obsahu
        
        
        for(let i = 0; i < alltasks.length; i++){ //generování tasků
            //target.innerHTML += "<div class=\"row\" id=\"task" + alltasks[i].id + "\"><div class=\"col\"><div class=\"card\"><div class=\"card-body\"><h5 class=\"card-title\">" + alltasks[i].name + "</h5><p class=\"card-text\">" + alltasks[i].description + "</p></div></div></div><div class=\"col\"></div></div>";
            $("#tasks").append("<div class=\"row\" id=\"task" + alltasks[i].id + "\"><div class=\"col\"><div class=\"card\"><div class=\"card-body\"><button type=\"button\" class=\"btn btn-light\" onclick=\"removeTask(" + alltasks[i].id + ")\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"16\" height=\"16\" fill=\"currentColor\" class=\"bi bi-trash\" viewBox=\"0 0 16 16\"><path d=\"M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z\"></path><path fill-rule=\"evenodd\" d=\"M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z\"></path></svg></button><h5 class=\"card-title\">" + alltasks[i].name + "</h5><p>" + alltasks[i].startdate + " - " + alltasks[i].enddate + "</p><p class=\"card-text\">" + alltasks[i].description + "</p><button type=\"button\" class=\"btn btn-light\" onclick=\"$('#newNote').modal('show');$('#taskId').val('" + alltasks[i].id + "')\" >Přidat poznámku</button></div></div></div><div class=\"col\"></div></div>");
            notes[i] = new ajax('/API/notes.php'); // stahování souvisejících poznámek
            notes[i].call({action: "getNotes",task: alltasks[i].id});
           // notes[i].call({action: "getNotes",task: alltasks[i].id}, (data) => {console.log(data)});
           // notes[i].call({action: "getNotes",task: alltasks[i].id, onSuccess: (data) => console.log(data) });
            
        }
        init = false;
        noteLoad = true;
    }
    if (noteLoad) {
        for(let m = 0; m < notes.length; m++) { //prochází všechna volání poznámek
            if (notes[m].response !== "null") {
                //alert(notes[m].response);
                let notesContent = JSON.parse(notes[m].response);
                //let notesContent = new Array();
                //notesContent = notes[m].response;
                //if (notesContent == "undefined") {
                    for(let y = 0; y < notesContent.length; y++) {
                        console.log(notesContent);
                        //notesContent = JSON.parse(notesContent);
                        $("#task"+notesContent[y].task).find(".col + .col").append("<div class=\"card\"><div class=\"card-body\"><button type=\"button\" class=\"btn btn-light\" onclick=\"removeNote(" + notesContent[y].id + ")\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"16\" height=\"16\" fill=\"currentColor\" class=\"bi bi-trash\" viewBox=\"0 0 16 16\"><path d=\"M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z\"></path><path fill-rule=\"evenodd\" d=\"M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z\"></path></svg></button><h5 class=\"card-title\">" + notesContent[y].name + "</h5><p class=\"card-text\">" + notesContent[y].description + "</p></div></div>");
                        //target = document.getElementById("task" + notesContent[y].id).children['2'];
                        //target.innerHTML += "notesContent";
                    }
                //}
            }
        }
    }
    //console.log(alltasks);
});



       
function login(event) {
    init = true;
    loginAttempt = true;
    user.call({action: "login", login: $("#loginname").val(), password: $("#password").val()});
    tasks.call({action: "getTasks"})
    return false;
}
function logout() {
    init = false;
    noteLoad = false;
    user.call({action: "logout"});
    //setTimeout(check, 200);
    setTimeout(function (){
        location.reload();
    }, 500); 
    return false;
}
function register(event) {    
    init = true;
    user.call({action: "register", login: $("#newlogin").val(), password: $("#newpassword").val(), email: $("#email").val(), jmeno: $("#jmeno").val(), prijmeni: $("#prijmeni").val(), rodnecislo: $("#rodnecislo").val()});
    return false;
}
function newTask() {
    tasks.call({action: "newTask", name: $("#taskName").val(), description: $("#taskDescription").val(), startdate: $("#startDate").val(), enddate: $("#endDate").val()});
    tasks.call({action: "getTasks"});
    init = true;
    $('#newTask').modal('hide');
    //location.reload(); 
    return false;
}
function newNote() {
    note.call({action: "newNote", name: $("#noteName").val(), description: $("#noteDescription").val(), task: $("#taskId").val()});
    tasks.call({action: "getTasks"});
    init = true;
    $('#newNote').modal('hide');
    //location.reload(); 
    return false;
}
function removeNote(noteId) {
    note.call({action: "removeNote", noteID: noteId});
    tasks.call({action: "getTasks"});
    init = true;
    return false;
}
function removeTask(taskId) {
    tasks.call({action: "removeTask", taskID: taskId});
    tasks.call({action: "getTasks"});
    init = true;
    return false;
}
</script>




<div id="userModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Nepřihlášený uživatel</h4>
        </div>
        <div class="modal-body">
          <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
              <a class="nav-link active" aria-current="page" data-bs-toggle="tab" id="login-tab" href="#login" role="tab" aria-controls="login" aria-selected="true">Log in</a>
            </li>
            <li class="nav-item" role="presentation">
              <a class="nav-link" href="#register" data-bs-toggle="tab" id="register-tab" role="tab" aria-controls="register" aria-selected="false">Register</a>
            </li>
          </ul>
          <div class="tab-content" id="myTabContent">
              <div class="tab-pane fade show active" id="login" role="tabpanel" aria-labelledby="login-tab">
                  <div id="loginform">
                      <label for="loginname">Login</label><input type="text" class="form-control" id="loginname"/>
                      <label for="password">Heslo</label><input type="password" class="form-control" id="password"/>
                      <br>
                      <button href="#" class="btn btn-primary" onclick="login();" id="sendLogin">Přihlásit</button>   
                  </div>    
              </div>
              <div class="tab-pane fade" id="register" role="tabpanel" aria-labelledby="register-tab">
                  <div id="registerform">
                      <label for="newlogin">Login</label><input type="text" id="newlogin" class="form-control"/>
                      <label for="newpassword">Heslo</label><input type="password" id="newpassword" class="form-control"/>
                      <label for="email">Email</label><input type="text" id="email" class="form-control"/>
                      <label for="jmeno">Jméno</label><input type="text" id="jmeno" class="form-control"/>
                      <label for="prijmeni">Přijmení</label><input type="text" id="prijmeni" class="form-control"/>
                      <label for="rodnecislo">Rodné číslo</label><input type="text" id="rodnecislo" class="form-control"/>
                      <br>
                      <button href="#" class="btn btn-primary" onclick="register();" id="sendRegister">Registrovat</button>   
                  </div>
              </div>
          </div>  


        </div>
        <!--<div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>-->
      </div>

    </div>
</div>



<div id="newTask" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Nový úkol</h4>
        </div>
        <div class="modal-body">
            <label for="taskName">Název</label><input type="text" id="taskName" class="form-control"/>
            <label for="taskDescription">Popis</label><textarea id="taskDescription" class="form-control"></textarea>
            <label for="startDate">Začátek</label><input type="text" class="datepicker form-control" id="startDate" />
            <label for="endDate">Konec</label><input type="text" class="datepicker form-control" id="endDate" />


        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" onclick="newTask();" data-dismiss="modal">Uložit úkol</button>
          <button type="button" class="btn btn-default" onclick="$('#newTask').modal('hide');" data-dismiss="modal">Zavřít</button>
        </div>
      </div>

    </div>
</div>

<div id="newNote" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Nová poznámka</h4>
        </div>
        <div class="modal-body">
            <label for="noteName">Název</label><input type="text" id="noteName" class="form-control"/>
            <label for="noteDescription">Popis</label><textarea id="noteDescription" class="form-control"></textarea>
            <input type="hidden" id="taskId"/>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" onclick="newNote();" data-dismiss="modal">Uložit poznámku</button>
          <button type="button" class="btn btn-default" onclick="$('#newNote').modal('hide');" data-dismiss="modal">Zavřít</button>
        </div>
      </div>

    </div>
</div>