<?php
include_once /*$_SERVER['HTTP_HOST'] . '/class/*/'config.php';
class notes {
    private $connection;
    function notes() {
        $connectionParameters = new databaseCredentials();
        $mysqli = new mysqli($connectionParameters -> serverName,$connectionParameters -> userName,$connectionParameters -> password,$connectionParameters -> dbName);
        if ($mysqli -> connect_errno) {
          echo "Failed to connect to MySQL: " . $mysqli -> connect_error;
          exit();
        }
        $this->connection = $mysqli;
    }
    
    function getNotes ($taskId) {
        //$result = $this->connection->query("SELECT notes.name, notes.description, notes.owner, notes.id, notes.task FROM tasks JOIN notes ON tasks.id=notes.owner WHERE notes.task = '$taskId'");
        $result = $this->connection->query("SELECT notes.name, notes.description, notes.owner, notes.id, notes.task FROM tasks JOIN notes ON tasks.id=notes.task WHERE notes.task = '$taskId'");
        while ($row = $result->fetch_object()){
                $tasks[] = $row;
            }
        return $tasks;
    }
    
    function createNote ($name, $description, $task) {
        $name = $this->connection->real_escape_string($name);
        $description = $this->connection->real_escape_string($description);
        $task = $this->connection->real_escape_string($task);
        if($result = $this->connection->query("INSERT INTO notes (name, description, task, owner) VALUES ('$name','$description','$task','" . $_SESSION['user']->id . "')")) {
            return TRUE;
        }
        else return FALSE;
    }
    function removeNote ($noteID) {
        $noteID = $this->connection->real_escape_string($noteID);
        if($result = $this->connection->query("DELETE FROM notes WHERE id = '$noteID'")) {
            return TRUE;
        }
        else return FALSE;
    }
}