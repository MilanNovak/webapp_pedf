<?php
include_once /*$_SERVER['REQUEST_URI'] . */'class/config.php';
$connectionParameters = new databaseCredentials();
$mysqli = new mysqli($connectionParameters -> serverName,$connectionParameters -> userName,$connectionParameters -> password);
$database = "CREATE DATABASE `" . $connectionParameters -> dbName . "` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci";

// Performs the $sql query on the server to create the database
if ($mysqli->query($database)) {
  echo 'Database "' . $connectionParameters -> dbName . '" successfully created';
}
else {
 echo 'Error: '. $mysqli->error;
}
if($mysqli->select_db($connectionParameters -> dbName)) {
    echo '<br> Connected to database successfully';
}
else {
 echo '<br>Error: '. $mysqli->error;
}

$tableCreation[] = "CREATE TABLE 'users' ('id' INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 'login' VARCHAR(30) NOT NULL, 'password' VARCHAR(60) NOT NULL, 'email' VARCHAR(50) NOT NULL, 'jmeno' VARCHAR(30), 'prijmeni' VARCHAR(30), 'rodnecislo' VARCHAR(10));";
$tableCreation[] = "CREATE TABLE 'tasks' ('id' INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 'name' VARCHAR(50) NOT NULL, 'startdate' DATETIME, 'enddate' DATETIME, 'description' TEXT, 'owner' INT(6));";
$tableCreation[] = "CREATE TABLE 'notes' ('id' INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 'name' VARCHAR(50) NOT NULL, 'description' TEXT, owner INT(6), 'task' INT(6));";
//$tableCreation[] = "CREATE TABLE taskstousers (task INT(6), user INT(6));";
//$tableCreation[] = "CREATE TABLE taskstonotes (task INT(6), notes INT(6));";

foreach ($tableCreation as &$value) {
    if ($mysqli->query($value)) {
        echo '<br>Table created';
    }
    else {
        echo '<br>Error: ' . $mysqli->error;
    }
}
$mysqli->close();
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

