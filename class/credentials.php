<?php
include_once /*$_SERVER['REQUEST_URI'] . */'config.php';

class credentials {
    private $connection;
    
    function credentials() {
        $connectionParameters = new databaseCredentials();
        $this->connection = new mysqli($connectionParameters -> serverName,$connectionParameters -> userName,$connectionParameters -> password,$connectionParameters -> dbName);
    }
    function register($login, $password, $email, $jmeno, $prijmeni, $rodnecislo) {
        $login = $this->connection->real_escape_string($login);
        $nameCheck = $this->connection->query("SELECT login FROM users WHERE login='$login'");
        if ($nameCheck->num_rows > 0) return FALSE;
        $password = password_hash($this->connection->real_escape_string($password), PASSWORD_DEFAULT);
        if($result = $this->connection->query("INSERT INTO users (login, password, email, jmeno, prijmeni, rodnecislo) VALUES ('$login', '$password', '$email', '$jmeno', '$prijmeni', '$rodnecislo')")) {
            $userInfo = $this->connection->query("SELECT * FROM users WHERE login = '$login'");
            while ($row = $userInfo->fetch_object()){
                unset($row->password);
                $_SESSION['user'] = $row;
                return $_SESSION['user'];
            }
        }
        else {
            return $this->connection->error;
        }
    }
    function login($login, $password) {
        $login = $this->connection->real_escape_string($login);
        $password = $this->connection->real_escape_string($password);
        if ($result = $this->connection->query("SELECT * FROM users WHERE login = '$login'")) {
            while ($row = $result->fetch_object()){
                if (password_verify($password, $row->password)) {
                    unset($row->password);
                    $_SESSION['user'] = $row;
                    return $_SESSION['user'];
                }
                else {
                    return FALSE;
                }
            }
            
        }
    }
    function check() {
        if (isset($_SESSION['user']) && $_SESSION['user']->login != '') return $_SESSION['user'];
        else return FALSE;
    }
    
    function logout() {
        session_unset();
        session_destroy();
        return FALSE;
    }
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

