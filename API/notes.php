<?php
session_start();
include_once /*$_SERVER['HTTP_HOST'] .*/ '../class/note.php';
$notes = new notes();
switch ($_POST['action']) {
    case "getNotes":
        echo json_encode($notes->getNotes($_POST['task']));
        break;
    case "newNote":
        $notes->createNote($_POST['name'], $_POST['description'], $_POST['task']);
        break;
    case "removeNote":
        $notes->removeNote($_POST['noteID']);
        break;
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

