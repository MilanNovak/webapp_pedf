<?php
session_start();
include_once /*$_SERVER['HTTP_HOST'] .*/ '../class/task.php';
$tasks = new tasks();
switch ($_POST['action']) {
    case "getTasks":
        echo json_encode($tasks->getTasks($_SESSION['user']->id));
        break;
    case "newTask":
        $tasks->createTask($_POST['name'], $_POST['description'], $_POST['startdate'], $_POST['enddate']);
        break;
    case "removeTask":
        $tasks->removeTask($_POST['taskID']);
        break;
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

